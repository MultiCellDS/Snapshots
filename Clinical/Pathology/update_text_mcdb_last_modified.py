#!/usr/bin/python

import MultiCellDS
import sys
import csv
from time import gmtime, strftime
import datetime
from xml.dom import minidom
from xml.dom.minidom import Node
import re

def remove_blanks(node):
    for x in node.childNodes:
        if not(x.nodeName == "text" or x.nodeName =="description") :
            if x.nodeType == Node.TEXT_NODE:
                if x.nodeValue:
                    x.nodeValue = x.nodeValue.strip()
            elif x.nodeType == Node.ELEMENT_NODE:
                remove_blanks(x)

patient_id_re = re.compile('patientid=')
single_quote_re = re.compile("'")
diagnosis_re = re.compile('diagnosis of ')
close_paren_re = re.compile('\)')

snapshot_id_dict = {}
with open('../../../Collections/MultiCellDB Snapshot ID numbers.csv', 'rb') as f:
    reader = csv.reader(f)
    reader.next() # Skip header row
    for row in reader:
        #print row
        row_entry = [row[1]]
        row_entry.extend(row[3])
        snapshot_id_dict[row[2]] = row_entry

for i in range(len(sys.argv)) :
    if i > 0 :
        xml = open(sys.argv[i]).read()

        result = MultiCellDS.CreateFromDocument(xml)

        #csvfile = open("../../Collections/MultiCellDB ID numbers.csv","r")

        name = result.metadata.MultiCellDB.name
        #label = result.label
        #old_citation_text = result.cell_line[0].metadata.citation.text
        MultiCellDB_name = ""
        MultiCellDB_ID = ""
        MultiCellDB_line = 0
        dict_name = ""
        #if name in snapshot_id_dict:
        #    dict_name = name
        #if label in snapshot_id_dict:
        #    dict_name = label
        #print(name, label, dict_name)
        dict_name = sys.argv[i]
        MultiCellDB_ID = snapshot_id_dict[dict_name][0]
        #MultiCellDB_line = snapshot_id_dict[dict_name][0]

        new_indent = '\n\t\t\t\t' # Four tabs
        citation_string = (new_indent+
                      'If this digital snapshot is used in a publication, cite it as:'+
                      new_indent+
                      ("'We used digital snapshot %s [1] version 1 (MultiCellDB_ID: %s) created with data and contributions from [2]'" % (dict_name[:-4], MultiCellDB_ID) ) +
                      new_indent +
                      "[1] S. H. Friedman et al., MultiCellDS: a standard and a community for sharing multicellular data (2016, submitted)" +
                      new_indent +
                      "[2] Dong F, Irshad H et al., Computational Pathology to Discriminate Benign from Malignant Intraductal Proliferations of the Breast. PLOS One (2014)" +
                      new_indent +
                      "[3] Dong F, Irshad H et al., Data from: Computational pathology to discriminate benign from malignant intraductal proliferations of the breast. Dryad Digital Repository (2014)."+
                      new_indent[:-1]) # The -1 is to delete the last tab for the end tag

        result.metadata.citation.text = citation_string
        result.metadata.citation.URL = "http://MultiCellDS.org"

        new_indent = '\n\t\t\t' # Three tabs

        old_description = result.metadata.description
        patient_ID_found = patient_id_re.search(old_description)
        single_quote_found = single_quote_re.search(old_description, patient_ID_found.end()+1)
        diagnosis_found = diagnosis_re.search(old_description)
        close_paren_found = close_paren_re.search(old_description, diagnosis_found.end())
        patient_ID = old_description[patient_ID_found.end():single_quote_found.start()+1]
        diagnosis_full = old_description[diagnosis_found.end():close_paren_found.start()+1]
        
        description_string = (new_indent +
                              ("This is the segmented H&amp;E image for patientid=%s from [1,2], with a diagnosis of %s. The current version includes nuclear morphometric parameters." % (patient_ID, diagnosis_full)) +
                              new_indent +
                              "For updated versions of this data, please see http://MultiCellDS.org"+
                              new_indent +
                              "[1] Dong F, Irshad H et al., Computational Pathology to Discriminate Benign from Malignant Intraductal Proliferations of the Breast. PLOS One (2014)" +
                              new_indent + 
                              "[2] Dong F, Irshad H et al., Data from: Computational pathology to discriminate benign from malignant intraductal proliferations of the breast. Dryad Digital Repository (2014)." +
                              new_indent[:-1]) # The -1 is to delete the last tab for the end tag

        result.metadata.description = description_string
        
        #result.metadata.curation.classification.classification_number = MultiCellDB_classification
        #result.metadata.curation.classification.line = MultiCellDB_line
        result.metadata.MultiCellDB.ID = MultiCellDB_ID
        result.metadata.MultiCellDB.name = sys.argv[i][:-4] # Omit ".xml" from the file name for a label
        last_modifier = result.metadata.curation.last_modified_by.orcid_identifier[0]
        last_modifier.path = "0000-0001-8003-6860"
        last_modifier.given_names = "Samuel"
        last_modifier.family_name = "Friedman"
        last_modifier.email = "friedman.samuel.h@gmail.com"
        last_modifier.url = "http://MathCancer.org"
        last_modifier.organization_name = "University of Southern California"
        last_modifier.department_name = "Lawrence J. Ellison Institute for Transformative Medicine of USC"

        time = datetime.datetime.now().isoformat()
        tz_str = strftime("%z", gmtime())
        tz_str = tz_str[:-2]+':'+tz_str[-2:] # Insert : before last two digits
        last_modified_time = time+tz_str
        result.metadata.curation.last_modified = last_modified_time

        # Make custom text pretty print out
        xml_ugly = result.toxml(encoding="utf-8")
        ugly_dom = minidom.parseString(xml_ugly)
        remove_blanks(ugly_dom)
        ugly_dom.normalize()
        xml_good = ugly_dom.toprettyxml(encoding="utf-8")

        fout = open(sys.argv[i],'w')
        #fout = open("check.xml", 'w')
        fout.write(xml_good)
        fout.close()

        fout = open(MultiCellDB_ID+".xml",'w')
        fout.write(xml_good)
        #fout.write(result.toDOM().toprettyxml())
        fout.close()
